
## Environment variables:

- SLACK_SECRET: The Slack signing secret key for this Slack project.
- GOOGLE_SERVICE_WORKER_MAIL: The email address of the Google service worker with whom the spreadsheet is shared.
- GOOGLE_SERVICE_WORKER_KEY: The Google service worker's private key.
- GOOGLE_SHEET_ID: The ID of the 980 spreadsheet.


## DynamoDB Tables

semaphor table item

```
{
  "group": "2",
  "id": "activeGroup"
}
```

people table item

```
{
  "group": "1",
  "name": "bob-kepford",
  "slackname": "bob.kepford"
}
```
