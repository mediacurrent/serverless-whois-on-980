'use strict';
let GoogleSpreadsheet;

const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

/**
 * Breaks array into an arrray with arrays of the given size.
 *
 * @param {array} items array to split.
 * @param {integer} chunkSize Size of every group.
 * @returns {array} tempArray
 */
function chunkArray(items, chunkSize) {
  let index = 0;
  const arrayLength = items.length;
  const tempArray = [];

  for (index = 0; index < arrayLength; index += chunkSize) {
    let myChunk = items.slice(index, index+chunkSize);
    tempArray.push(myChunk);
  }

  return tempArray;
}

/**
 * batchWrite to DynamboDb
 *
 * @param {object} items Data to update.
 * @param {object} callback Lambda callback.
 * @returns {object} response
 */
function batchWrite (items, callback) {
  const peopleTable = process.env.PEOPLE_TABLE;

  // Split in group of 24 items
  var batchedItems = chunkArray(items, 24);

  const batchUpdate = batchedItems.map((batchedItem) => {
    const params = {
      RequestItems: {
        [peopleTable]: batchedItem
      }
    };
    dynamoDb.batchWrite(params, function(err, data) {
      if (err) {
        console.log('Error', err);
      }
      else {
        const response = {
          statusCode: 200,
          body: JSON.stringify({
            text: 'Synced data with the spreadsheet.',
            data: data
          })
        };
        callback(null, response);
      }
    });
  });
  return batchUpdate;
}

/**
 *  Scheduled sync of data.
 *  For more info read https://serverless.com/framework/docs/providers/aws/events/schedule/
 * @param {object} event Lambda event.
 * @param {object} context Lambda context.
 * @param {object} callback Lambda callback.
 * @returns {object} response
 */
module.exports.syncSheetToDynamodb = (event, context, callback) => {
  var async = require('async');
  GoogleSpreadsheet = GoogleSpreadsheet || require('google-spreadsheet');
  let sheet;
  const docId = process.env.GOOGLE_SHEET_ID;
  const doc = new GoogleSpreadsheet(docId);
  let userData = {};
  let items = [];
  async.series([
    step => {

      const docCreds = {
        client_email: process.env.GOOGLE_SERVICE_WORKER_MAIL,
        private_key: process.env.GOOGLE_SERVICE_WORKER_KEY.replace(/\\n/g, '\n'),
      };

      return doc.useServiceAccountAuth(docCreds, step);
    },
    step => {
      // Get the sheet.
      return doc.getInfo((err, info) => {
        if (!err) {
          sheet = info.worksheets[0];
        }

        step();
      });
    },
    step => {
      // Write back to Firebase.
      return sheet.getCells({
        'min-row': 2,
        'return-empty': false,
      }, (err, cells) => {

        // Maps to the column in the sheet.
        const colMap = {
          7: 'slackname',
          1: 'name',
          6: 'group',
        };

        if (!err) {
          // First, assemble into an object.
          cells.forEach((cell, index) => {
            let prop = colMap[cell.col];
            if (!prop) {
              return;
            }
            if (!userData[cell.row]) {
              userData[cell.row] = {};
            }
            // Remove line breaks.
            userData[cell.row][prop] = cell.value.replace(/\n/g, '');
          });

          // Then re-key by slackname.
          Object.keys(userData).forEach(key => {
            let user = userData[key];
            if (!user.slackname || !user.group || !user.name) {
              return;
            }
            items.push(
              {
                PutRequest: {
                  Item: {
                    name: '',
                    group: '',
                    ...user,
                  }
                }
              }
            );
            delete userData[key];
          });
        }
        step();
      });
    }
  ], (err) => {
    if (err) {
      return console.error(err);
    }
    else {
      // Write sheet data to DynamoDB.
      return batchWrite(items, callback);
    }
  });
};
