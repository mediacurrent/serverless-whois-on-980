'use strict';

const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const getActiveGroup = require('./utils/getActiveGroup');

/**
 * Switch active 980 group at 12:00 AM every Saturday.
 * @param {object} event Lambda event.
 * @param {object} context Lambda context.
 * @returns {object} response
 */
exports.switchActiveGroup = async (event, context) => {

  try {
    // Change active 980 group.
    const activeGroup = await getActiveGroup();
    const newActiveGroup = activeGroup === '1' ? '2' : '1';

    // Increment number by one.
    const params = {
      TableName: process.env.SEMAPHOR_TABLE,
      Item:{
        'id': 'activeGroup',
        'group': newActiveGroup
      }
    };

    const data = await dynamoDb.put(params).promise();
    console.log(`Set active group to ${newActiveGroup}`);
    return {
      statusCode: 200,
      body: JSON.stringify({ params, data })
    };
  }
  catch (error) {
    return {
      statusCode: 400,
      error: `Could not change active group: ${error.stack}`
    };
  }
};

