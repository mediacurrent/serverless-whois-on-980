'use strict';

const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

/**
 * Get a person's data from Firebase.
 *
 * @param {string} slackname A slack name.
 * @returns {string} The person's slackname.
 */
async function getPerson(slackname) {

  const params = {
    TableName: process.env.PEOPLE_TABLE,
    Key: {
      'slackname': slackname
    }
  };

  try {
    const data = await dynamoDb.get(params).promise();
    const person = data.Item;
    return person;
  }
  catch (err) {
    return console.error('Unable to read item. Error JSON:', JSON.stringify(err, null, 2));

  }
}
module.exports = getPerson;
