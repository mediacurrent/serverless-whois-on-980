'use strict';

const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

/**
 * Get the active group from DynamoDB.
 * @returns {string} Group number as a string.
 */
async function getActiveGroup() {
  const params = {
    TableName: process.env.SEMAPHOR_TABLE,
    Key: {
      'id': 'activeGroup'
    }
  };
  try {
    const data = await dynamoDb.get(params).promise();
    const group = data.Item.group;
    return group;
  }
  catch (err) {
    return console.error('Unable to read item. Error JSON:', JSON.stringify(err, null, 2));
  }
}

module.exports = getActiveGroup;
