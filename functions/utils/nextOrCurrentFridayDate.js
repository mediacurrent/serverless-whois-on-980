'use strict';
let format;

/**
 * Get the date of the next or current Friday.
 * @returns {string} date of the next or currrent Friday.
 */
module.exports = () => {
  format = format || require('date-fns/format');

  const day = 5;
  let now = new Date();
  now.setDate(now.getDate() + (day+(7-now.getDay())) % 7);
  return format(now, 'dd MMMM yyyy');
};
