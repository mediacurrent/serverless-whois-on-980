'use strict';

/**
 * Verify a request based on its slack secret.
 * @param {object} req Lambda event object.
 * @returns {boolean} Is the request valid or not.
 */
module.exports = (req) => {
  if (process.env.IS_LOCAL) {
    console.log('Local invocation');
    return true;
  }
  const timingSafeCompare = require('tsscmp');
  const crypto = require('crypto');

  const signature = req.headers['X-Slack-Signature'];
  const timestamp = req.headers['X-Slack-Request-Timestamp'];
  const slackSecret = process.env.SLACK_SECRET;
  const hmac = crypto.createHmac('sha256', slackSecret);
  const [version, hash] = signature.split('=');

  // Check if the timestamp is too old
  const fiveMinutesAgo = ~~(Date.now() / 1000) - (60 * 5);
  if (timestamp < fiveMinutesAgo) {
    console.log('Request timestamp was too old.');
    return false;
  }
  hmac.update(`${version}:${timestamp}:${req.body}`);

  // Check that the request signature matches expected value
  return timingSafeCompare(hmac.digest('hex'), hash);
};

