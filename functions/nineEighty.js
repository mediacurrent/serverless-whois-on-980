'use strict';

const nextOrCurrentFridayDate = require('./utils/nextOrCurrentFridayDate');
const isVerified = require('./utils/isVerified');
const getPerson = require('./utils/getPerson');
const getActiveGroup = require('./utils/getActiveGroup');

/**
 *  Determine if a person is on 980 this Friday.
 * @param {object} event Lambda event.
 * @param {object} context Lambda context.
 * @returns {object} response
 */
module.exports.nineEighty = async (event, context) => {
  const validReq = isVerified(event);
  if (!validReq) {
    return {
      statusCode: 401,
      body: 'Request not authorized'
    };
  }

  const qs = require('querystring');
  const payload = qs.parse(event.body);

  // Support but don't require a leading @.
  let slackname = payload.text;
  if (slackname.charAt(0) === '@') {
    slackname = slackname.substr(1);
  }

  const activeGroup = await getActiveGroup();
  const person = await getPerson(slackname);

  if (!activeGroup) {
    console.error('activeGroup not set');
  }
  if (!person || !person.group || person.group === '0') {
    return {
      statusCode: 200,
      body: `${slackname} is not in the 980 schedule.`
    };
  }

  let message;
  if (person.group === activeGroup) {
    message = `*${person.name || slackname}* is :palm_tree: *off* this Friday, ${nextOrCurrentFridayDate()}.`;
  }
  else {
    message = `*${person.name || slackname}* is :computer: *working* this Friday, ${nextOrCurrentFridayDate()}.`;
  }
  let body = JSON.stringify({
    text: message
  });

  return {
    statusCode: 200,
    body
  };
};
